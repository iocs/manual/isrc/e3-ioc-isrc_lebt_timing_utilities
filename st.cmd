require essioc

iocshLoad("$(essioc_DIR)/common_config.iocsh")

epicsEnvSet("EVRPREFIX", "ISrc-TimCon:Ctrl-EVR-101:")
epicsEnvSet("TIMCON_P", "ISrc-TimCon:")

epicsEnvSet("MAGNETRON_R", "SC-IOC-001:Magtr")

dbLoadRecords("$(E3_CMD_TOP)/db/timingdesc.db", "P=$(TIMCON_P), R=$(MAGNETRON_R), EVRPREFIX=$(EVRPREFIX), STRTEVT=IonMagSt, ENDEVT=IonMagEnd")
dbLoadRecords("$(E3_CMD_TOP)/db/magnetrontimok.db", "P=$(TIMCON_P), R=$(MAGNETRON_R), EVRPREFIX=$(EVRPREFIX), FFNUM=4, STRTEVTNUM=10, ENDEVTNUM=11, STRTPUL=8, ENDPUL=9")


epicsEnvSet("CHOPPER_R", "SC-IOC-001:LEBTChop")

dbLoadRecords("$(E3_CMD_TOP)/db/choppertimok.db", "P=$(TIMCON_P), R=$(CHOPPER_R), EVRPREFIX=$(EVRPREFIX), STRTEVTNUM=6, ENDEVTNUM=7, STRTPUL=10, ENDPUL=11")
dbLoadRecords("$(E3_CMD_TOP)/db/timingdesc.db", "P=$(TIMCON_P), R=$(CHOPPER_R), EVRPREFIX=$(EVRPREFIX), STRTEVT=LEBTCSt, ENDEVT=LEBTCEnd")

iocInit()
